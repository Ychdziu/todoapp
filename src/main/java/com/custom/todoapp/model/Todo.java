package com.custom.todoapp.model;

import java.sql.Timestamp;

public class Todo {

	private int todId;
	private String todText;
	private Timestamp todCreationDate;
	private String todIsActive;
	
	public Todo(int todId, String todText, Timestamp todCreationDate, String todIsActive) {
		this.setTodId(todId);
		this.setTodText(todText);
		this.setTodCreationDate(todCreationDate);
		this.setTodIsActive(todIsActive);
	}

	public int getTodId() {
		return todId;
	}

	public void setTodId(int todId) {
		this.todId = todId;
	}

	public String getTodText() {
		return todText;
	}

	public void setTodText(String todText) {
		this.todText = todText;
	}

	public Timestamp getTodCreationDate() {
		return todCreationDate;
	}

	public void setTodCreationDate(Timestamp todCreationDate) {
		this.todCreationDate = todCreationDate;
	}

	public String getTodIsActive() {
		return todIsActive;
	}

	public void setTodIsActive(String todIsActive) {
		this.todIsActive = todIsActive;
	}
	
}
