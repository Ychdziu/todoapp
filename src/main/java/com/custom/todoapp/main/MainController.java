package com.custom.todoapp.main;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.custom.todoapp.model.Todo;
import com.custom.todoapp.util.JdbiTodoRepository;
import com.custom.todoapp.util.JdbiUtil;

@RestController
public class MainController {

	JdbiTodoRepository todoRepo = new JdbiTodoRepository(JdbiUtil.getJdbi());

	@GetMapping(value = "/api/todosActive")
	@CrossOrigin(origins = "http://localhost:3000")
	public List<Todo> getAllTodosActive() {
		List<Todo> todos = todoRepo.findAllActive();
		return todos;
	}

	@GetMapping(value = "/api/todosArchived")
	@CrossOrigin(origins = "http://localhost:3000")
	public List<Todo> getAllTodosArchived() {
		List<Todo> todos = todoRepo.findAllArchived();
		return todos;
	}

	@GetMapping(value = "/api/todo/{id}")
	@CrossOrigin(origins = "http://localhost:3000")
	public Optional<Todo> getTodo(@PathVariable("id") int todId) {
		Optional<Todo> todo = todoRepo.findById(todId);
		return todo;
	}

	@GetMapping(value = "/api/todoDone/{id}")
	@CrossOrigin(origins = "http://localhost:3000")
	public Optional<Todo> setTodDone(@PathVariable("id") int todId) {
		Optional<Todo> result = todoRepo.setTodDone(todId);
		return result;
	}

	@GetMapping(value = "/api/todoSave/{text}")
	@CrossOrigin(origins = "http://localhost:3000")
	public Optional<Todo> saveTodo(@PathVariable("text") String todText) {
		Optional<Todo> result = todoRepo.saveTod(todText);
		return result;
	}
}
