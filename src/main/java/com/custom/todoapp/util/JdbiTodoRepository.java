package com.custom.todoapp.util;

import java.util.List;
import java.util.Optional;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.custom.todoapp.model.Todo;

@Repository
@Transactional
public class JdbiTodoRepository implements TodoRepository {

	private static final String INSERT_TODO_QUERY = "INSERT INTO Todos(tod_text, tod_creation_date) VALUES (:tod_text, sysdate());";
	private static final String SELECT_TODOS_ACTIVE_QUERY = "SELECT tod_id, tod_text, tod_creation_date, tod_is_active FROM Todos where tod_is_active = 'Y'";
	private static final String SELECT_TODOS_ARCHIVED_QUERY = "SELECT tod_id, tod_text, tod_creation_date, tod_is_active FROM Todos where tod_is_active = 'N'";
	private static final String SELECT_TODO_QUERY = "SELECT tod_id, tod_text, tod_creation_date, tod_is_active FROM Todos WHERE tod_id=:tod_id";
	private static final String UPDATE_TODO_QUERY = "call set_tod_done(:tod_id)";
	private static final String SELECT_LAST_TODO = "select tod_id, tod_text, tod_creation_date, tod_is_active from Todos where tod_id = (select max(tod_id) from Todos where tod_is_active = 'Y')";
	private final Jdbi jdbi;

	public JdbiTodoRepository(Jdbi jdbi) {
		this.jdbi = jdbi;
	}

	@Override
	public List<Todo> findAllActive() {
		List<Todo> result = null;
		try (Handle handle = jdbi.open()) {
			result = handle.createQuery(SELECT_TODOS_ACTIVE_QUERY).mapTo(Todo.class).list();
		}
		return result;
	}

	@Override
	public List<Todo> findAllArchived() {
		List<Todo> result = null;
		try (Handle handle = jdbi.open()) {
			result = handle.createQuery(SELECT_TODOS_ARCHIVED_QUERY).mapTo(Todo.class).list();
		}
		return result;
	}

	@Override
	public Optional<Todo> findById(int id) {
		Optional<Todo> result = null;
		try (Handle handle = jdbi.open()) {
			result = handle.createQuery(SELECT_TODO_QUERY).bind("tod_id", id).mapTo(Todo.class).findFirst();
		}

		return result;
	}

	@Override
	public Optional<Todo> saveTod(String todText) {
		int dummy = 0;
		try (Handle handle = jdbi.open()) {
			dummy = handle.createUpdate(INSERT_TODO_QUERY).bind("tod_text", todText).execute();
		}
		;
		Optional<Todo> newTodo = null;
		if (dummy > 0) {
			try (Handle handle = jdbi.open()) {
				newTodo = handle.createQuery(SELECT_LAST_TODO).mapTo(Todo.class).findFirst();
			}
		}

		return newTodo;
	}

	@Override
	public Optional<Todo> setTodDone(int id) {
		boolean called = false;
		Optional<Todo> todo = null;
		try (Handle handle = jdbi.open()) {
			handle.createCall(UPDATE_TODO_QUERY).bind("tod_id", id).invoke();
			called = true;

		}

		if (called)
			todo = findById(id);

		return todo;
	}

}
