package com.custom.todoapp.util;

import java.util.List;
import java.util.Optional;

import com.custom.todoapp.model.Todo;

public interface TodoRepository {

	List<Todo> findAllActive();
	List<Todo> findAllArchived();
    Optional<Todo> findById(int id);

    Optional<Todo> saveTod(String todText);
    Optional<Todo> setTodDone(int id);
	
}
