package com.custom.todoapp.util;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.reflect.ConstructorMapper;
import org.springframework.context.annotation.Bean;

import com.custom.todoapp.model.Todo;

public class JdbiUtil {

	@Bean
	public static Jdbi getJdbi() {

		String userName = "custom_user";
		String password = "CodingInventi";
		Jdbi jdbi = Jdbi.create("jdbc:mysql://localhost:3306/custom", userName, password);

		jdbi.registerRowMapper(Todo.class, ConstructorMapper.of(Todo.class));

		return jdbi;
	}

}
