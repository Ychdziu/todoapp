CREATE DATABASE `custom` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
CREATE TABLE `custom`.`todos` (
  `tod_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `tod_text` varchar(100) NOT NULL COMMENT 'Todo text',
  `tod_creation_date` datetime NOT NULL COMMENT 'Todo record created',
  `tod_is_active` varchar(1) NOT NULL DEFAULT 'Y' COMMENT 'Is record active',
  `tod_last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`tod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Main ToDo table';
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `set_tod_done`(in p_tod_id int)
BEGIN
  update todos 
  set tod_is_active = 'N',
      tod_last_updated = sysdate()
  where tod_id = p_tod_id;
END$$
DELIMITER ;