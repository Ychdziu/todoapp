import React, { Component } from 'react';
import './App.css';
import Todos from './components/Todos';
import { Link } from 'react-router-dom'
import todoList from './images/todoList.png';
import archive from './images/archive.png';
import AddTodo from './components/AddTodo';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import TodoArchived from './components/TodoArchived';
import axios from 'axios';


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            todos: [],
            todosArchived: []
        }
        console.log(this.state.todos);
    }

    // fetch data
    componentDidMount() {
        axios.get('http://localhost:9000/api/todosActive')
            .then(res => this.setState({ todos: res.data }));

        axios.get('http://localhost:9000/api/todosArchived')
            .then(res => this.setState({ todosArchived: res.data }));
    }
    
    // Set Todo done/archived
    setTodDone = (todId) => {
    	axios.get(`http://localhost:9000/api/todoDone/${todId}`,
        		{id: todId}).then(res => this.setState({ todosArchived: [...this.state.todosArchived, res.data] }));
        this.setState({todos: [...this.state.todos.filter(todo => todo.todId !== todId)]})
    }

    // Add Todo
    addTodo = (title) => {
        console.log("Creating new with title: "+title);
        axios.get(`http://localhost:9000/api/todoSave/${title}`,
        		{text: title}).then(res => this.setState({ todos: [...this.state.todos, res.data] }));
    }

    render() {
        return (
            <Router>
                <div className="App">
                    <div className="container">
                        <div style={upperStyle}>
                            <header style={headerStyle}>
                                <div>
                                    <p style={firstTabStyle}>
                                            <Link style={linkStyle} to="/">
                                                    <img src={todoList} style={imgStyle} alt="todoList" />
                                            {' '} Todo list
                                            </Link>
                                    </p>
                                    <p style={secondTabStyle}>
                                            <Link style={linkStyle} to="/todoArchived" >
                                            <img src={archive} style={imgStyle} alt="archive" />
                                            {' '} Archived
                                            </Link>
                                    </p>
                                </div>
                            </header>
                            <h3 style={rightLineStyle}/>
                            <Route exact={true} path="/" render={props => (
                                <React.Fragment>
                                    <div style={fragmentStyle}>
                                        <h1>TodoList</h1>
                                        <Todos todos={this.state.todos}
                                            setTodDone={this.setTodDone} />
                                        <AddTodo addTodo={this.addTodo}/>
                                        </div>
                                </React.Fragment>)} />
                            <Route path="/todoArchived" render={
                                props => (
                                    <React.Fragment>
                                        <div style={fragmentStyle}>
                                            <h1>TodoList archived</h1>
                                            <TodoArchived todos={this.state.todosArchived}/>
                                        </div>
                                    </React.Fragment>)}/>
                        </div>
                    </div>
                </div>
            </Router>
        );
    }
}

const headerStyle = {
    textAlign: 'center',
    padding: '10px'
}

const linkStyle = {
    textDecoration: 'none'
}

const upperStyle = {
    display: 'flex',
    marginLeft: 10,
    marginRight: 100
}

const firstTabStyle = {
    width: 120,
    marginBottom: 10,
    borderBottom: 'solid 1px black',
    padding: '10px',
    display: 'flex'
}

const secondTabStyle = {
    width: 120,
    marginBottom: 10,
    padding: '10px',
    display: 'flex'
}

const imgStyle = {
    height: 30,
    width: 30
}

const rightLineStyle = {
    marginRigth: 10,
    marginLeft: 10,
    borderRight: 'solid 2px black'
}

const fragmentStyle = {
    marginRigth: 10,
    marginLeft: 10
}

export default App;
