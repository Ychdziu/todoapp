import React, { Component } from 'react';
import TodoArchivedItem from './TodoArchivedItem';

class TodoArchived extends Component{

    render() {
        return this.props.todos.map((todo) => (
            <div style={{
                marginBottom: 10,
                borderleft: 'solid 2px black',
                padding: '1px'
            }}>
                <TodoArchivedItem key={todo.todId} todo={todo} />
            </div>
        ));
    }
}

export default TodoArchived;