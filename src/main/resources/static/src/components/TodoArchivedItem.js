import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TodoArchivedItem extends Component {

    getStyle = () => {
        return {
            background: '#f4f4f4',
            padding: '10px',
            border: '1px black solid',
            width: '600px'
        }
    }

    render() {
    	const  { todId,  todText, todCreationDate } = this.props.todo;
        return (
            <div style={this.getStyle()}>
            	<div>
            		{todText}
            		<div style={{
            			display: 'flex',
            			float: 'right'}}>
            		{todCreationDate.substring(0,10)}
            		</div>
            	</div>
            </div>);
    }
}

export default TodoArchivedItem;
