import React, { Component } from 'react';
import Modal from 'react-awesome-modal';

class AddTodo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            visible: false
        }
    }

    openModal() {
        this.setState({
            visible: true
        });
    }

    closeModal() {
        this.setState({
            visible: false
        });
    }

    onChange = (e) => {
        console.log(e.target.value);
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.addTodo(this.state.title);
        this.setState({ title: '' });
        this.closeModal();
    }

    render() {
        return (
            <section>
                <input type="button" value="Add Todo" onClick={() => this.openModal()} />
                <Modal
                    visible={this.state.visible}
                    width="400"
                    height="300"
                    effect="fadeInUp"
                >
                    <div>
                        <form onSubmit={this.onSubmit} style={{ display: 'flex' }}>
                            <input
                                type="text"
                                name="title"
                                style={{ flex: '10', padding: '5px' }}
                                placeholder="Add Todo ..."
                                value={this.state.title}
                                onChange={this.onChange}
                            />
                            <input
                                type="submit"
                                value="Submit"
                                className="btn"
                                style={{ flex: '1' }}
                            />
                        </form>
                        <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
                    </div>
                </Modal>
            </section>
        );
    }
}

export default AddTodo;
