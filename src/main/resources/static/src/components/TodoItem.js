import React, { Component } from 'react';
import PropTypes from 'prop-types';
import archive from '../images/archive.png';

class TodoItem extends Component {

    getStyle = () => {
        return {
            background: '#f4f4f4',
            padding: '10px',
            border: '1px black solid',
            width: '600px'
        }
    }
    
    render() {
        const  { todId,  todText, todCreationDate } = this.props.todo;
        return (
            <div style={ this.getStyle() }>
                <div>
                    {todText}
                    <div style={{
                    	display: 'flex',
                    	float: 'right'
                    }}>
                    	{todCreationDate.substring(0,10)}
                    	<img src={archive} style={btnStyle} alt="archive" onClick={this.props.setTodDone.bind(this, todId)}/>
                    </div>
                </div>
            </div>);
    }
}
            
const btnStyle = {
    height: 20,
    width: 20,
    border: 'none',
    padding: '5xp 8px',
    float: 'right'
}

export default TodoItem;
