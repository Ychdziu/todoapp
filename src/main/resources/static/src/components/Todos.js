import React, { Component } from 'react';
import TodoItem from './TodoItem';
import PropTypes from 'prop-types';

class Todos extends Component {

    render() {
    	console.log(this.props.todos);
        return this.props.todos.map((todo) => (
            <div style={{
                marginBottom: 10,
                borderleft: 'solid 2px black',
                padding: '1px'
            }}>
                <TodoItem key={todo.todId} todo={todo} setTodDone={this.props.setTodDone} />
            </div>
            ));
    }
}

export default Todos;
